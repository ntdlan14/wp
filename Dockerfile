# Use the official php image as the base
FROM php:7.4-fpm

# Set working directory
WORKDIR /var/www/html

# Copy WordPress files
COPY wordpress/ .

# Expose the default WordPress port
EXPOSE 80

# Start php-fpm process
CMD ["php-fpm"]